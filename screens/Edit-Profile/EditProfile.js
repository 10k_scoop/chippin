import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  TextInput,
  TouchableOpacity,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import Header from "./components/Header";
import BottomMenu from "../../components/BottomMenu";

export default function EditProfile({ navigation }) {
  return (
    <View style={styles.container}>
      <Header goBack={() => navigation.goBack()} />

      <View style={styles.Bubble}>
        <Image source={require("../../assets/Bubble.png")} />
      </View>
      <ScrollView>
        <View style={styles.Wrapper}>
          <View style={styles.Profile}>
            <View style={styles.Img}>
              <Image
                source={require("../../assets/Profile.jpg")}
                style={{ width: "100%", height: "100%" }}
                resizeMode="cover"
              />
            </View>
          </View>
          <View style={styles.NameRow}>
            <Text>Name</Text>
            <TextInput style={styles.TextField} />
          </View>
          <View style={styles.AboutRow}>
            <Text>About</Text>
            <TextInput style={styles.AboutField} />
          </View>
          <View style={styles.NameRow}>
            <Text>Contact info</Text>
            <TextInput style={styles.TextField} />
          </View>
          <View style={styles.NameRow}>
            <Text>Location</Text>
            <TextInput style={styles.TextField} />
          </View>
          <TouchableOpacity style={styles.Btn}>
            <Text style={styles.BtnTxt}>Edit Profile</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.ChangePassorwd}>
            <Text style={{ color: "blue", fontSize: rf(14) }}>
              Change Passorwd
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
      <BottomMenu navigation={navigation} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#e3f7ff",
    alignItems: "center",
  },
  Wrapper: {
    width: wp("100%"),
    alignItems: "center",
    marginBottom: hp("8%"),
  },
  Bubble: {
    position: "absolute",
    right: 0,
    top: -60,
    right: -60,
  },
  Profile: {
    width: wp("100%"),
    height: hp("16%"),
    alignItems: "center",
    justifyContent: "center",
  },
  Img: {
    width: wp("20%"),
    height: wp("20%"),
    borderRadius: 100,
    overflow: "hidden",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 20,
  },
  NameRow: {
    width: wp("90%"),
    height: hp("10%"),
    justifyContent: "space-between",
    marginBottom: 10,
  },
  TextField: {
    width: "100%",
    height: "60%",
    backgroundColor: "#fff",
    borderRadius: 10,
  },
  AboutRow: {
    width: wp("90%"),
    height: hp("22%"),
    justifyContent: "space-evenly",
    marginBottom: 10,
  },
  AboutField: {
    width: "100%",
    height: "70%",
    backgroundColor: "#fff",
    borderRadius: 10,
  },
  Btn: {
    width: wp("90%"),
    height: hp("6%"),
    backgroundColor: "blue",
    borderRadius: 10,
    marginTop: "5%",
    alignItems: "center",
    justifyContent: "center",
  },
  BtnTxt: {
    fontSize: rf(14),
    color: "#fff",
  },
  ChangePassorwd: {
    width: wp("90%"),
    height: hp("6%"),
    backgroundColor: "#fff",
    borderRadius: 10,
    marginTop: "5%",
    alignItems: "center",
    justifyContent: "center",
  },
});
