import React from "react";
import { StyleSheet, Text, View, TouchableOpacity, Image } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { AntDesign } from "@expo/vector-icons";
export default function Header(props) {
  return (
    <View style={styles.container}>
      <View style={styles.title}>
        <Image
          source={require("../../../assets/LogoPic.png")}
          style={{ width: "50%", height: "45%", marginRight: "7%" }}
          resizeMode="contain"
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("100%"),
    height: hp("10%"),
    paddingVertical: 5,
  },
  title: {
    width: "100%",
    height: "100%",
    alignItems: "center",
    justifyContent: "flex-end",
  },
});
