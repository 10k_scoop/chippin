import React from "react";
import { StyleSheet, Text, View, TouchableOpacity, Image } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { Entypo } from "@expo/vector-icons";
export default function Card(props) {
  return (
    <View style={styles.container}>
      <View style={styles.FirstRow}>
        <Text style={styles.Font1}>Red Cross Driver</Text>
        <Entypo name="drop" size={rf(22)} color="#dc0203" />
      </View>
      <View style={styles.SecondRow}>
        <Text style={styles.font2}>
          Lorem ipsum dolor sit amet, consectetur{"\n"}adipisicing elit, sed do
          eiusmod tempor incididunt{"\n"}ut labore et dolore magna aliqua.
        </Text>
      </View>
      <TouchableOpacity style={styles.ThirdRow}>
        <Text style={styles.font3}>{props.btntxt}</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("90%"),
    height: hp("21%"),
    backgroundColor: "#fff",
    borderRadius: 10,
    marginBottom: "3%",
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
  FirstRow: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
    height: "25%",
    alignItems: "center",
    paddingHorizontal: "2%",
  },
  Font1: {
    fontSize: rf(18),
  },
  SecondRow: {
    width: "100%",
    height: "50%",
    justifyContent: "center",
    paddingHorizontal: "2%",
  },
  font2: {
    fontSize: rf(16),
    color: "grey",
  },
  ThirdRow: {
    width: "97%",
    height: "20%",
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#00f700",
  },
  font3: {
    color: "#fff",
    fontWeight: "700",
    fontSize: rf(14),
  },
});
