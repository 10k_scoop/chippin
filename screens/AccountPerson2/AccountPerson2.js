import React from "react";
import { StyleSheet, Text, View, Image, ScrollView } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import Header from "./components/Header";
import Card from "./components/Card";
import BottomMenu from "../../components/BottomMenu";

export default function AccountPerson2({ navigation }) {
  return (
    <View style={styles.container}>
      <Header goBack={() => navigation.goBack()} />

      <View style={styles.Bubble}>
        <Image source={require("../../assets/Bubble.png")} />
      </View>
      <ScrollView>
        <View style={styles.Wrapper}>
          <View style={styles.Discription}>
            <Text style={styles.titleFont}>Upcoming</Text>
            <Text style={styles.discFont}>
              There are many ways to serve your community, find oppurtunities to
              give back in your area below.
            </Text>
          </View>
          <Card btntxt="confirmed" sunny backgroundColor="#d77eea" />
          <Card btntxt="confirmed" hand backgroundColor="#ff8058" />
          <Card btntxt="confirmed" hand backgroundColor="#ff8058" />
        </View>
      </ScrollView>
      <BottomMenu navigation={navigation} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#e3f7ff",
    alignItems: "center",
  },
  Wrapper: {
    width: wp("100%"),
    alignItems: "center",
    marginBottom: hp("8%"),
  },
  Bubble: {
    position: "absolute",
    right: 0,
    top: -60,
    right: -60,
  },
  Discription: {
    width: wp("90%"),
    height: hp("15%"),
    justifyContent: "center",
  },
  titleFont: {
    fontSize: rf(20),
    fontWeight: "700",
    marginBottom: 10,
  },
  discFont: {
    fontSize: rf(14),
    fontWeight: "400",
  },
  BgImage: {
    flex: 1,
    justifyContent: "flex-end",
  },
});
