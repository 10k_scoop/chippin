import React from "react";
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Image,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import Header from "./components/Header";
import TextField from "./components/TextField";

export default function Event1({ navigation }) {
  return (
    <View style={styles.container}>
      <Header goBack={() => navigation.goBack()} />

      <View style={styles.Bubble}>
        <Image source={require("../../assets/Bubble.png")} />
      </View>

      <View style={styles.Wrapper}>
        <View style={styles.Discription}>
          <Text style={styles.titleFont}>Create Charity Account</Text>
          <Text style={styles.discFont}>
            Create a Chipin Charity Account and find oppurtunites to volunteer
            today
          </Text>
        </View>
        <TextField txt="Name your event" />
        <TextField txt="Date of event" />
        <TextField txt="Date of event" />

        <ImageBackground
          source={require("../../assets/BgPic.png")}
          style={styles.Image}
          resizeMode="contain"
        >
          <TouchableOpacity
            style={styles.Btn}
            onPress={() => navigation.navigate("Event2")}
          >
            <Text style={styles.btnFont}>next</Text>
          </TouchableOpacity>
          <View style={{ width: "80%", alignItems: "center" }}>
            <Text style={{ color: "grey" }}>step 1 of 3</Text>
          </View>
        </ImageBackground>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#e3f7ff",
  },
  Wrapper: {
    width: wp("100%"),
    alignItems: "center",
  },
  Bubble: {
    position: "absolute",
    right: 0,
    top: -60,
    right: -70,
  },
  Discription: {
    width: wp("90%"),
    height: hp("15%"),
    justifyContent: "center",
  },
  titleFont: {
    fontSize: rf(22),
    fontWeight: "700",
    marginBottom: 10,
  },
  discFont: {
    fontSize: rf(16),
    fontWeight: "400",
  },
  Image: {
    width: wp("100%"),
    height: hp("50%"),
    alignItems: "center",
    justifyContent: "center",
  },
  Btn: {
    width: wp("90%"),
    height: hp("6%"),
    backgroundColor: "#fff",
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 10,
    marginTop: "10%",
  },
  btnFont: {
    fontSize: rf(16),
    color: "blue",
  },
});
