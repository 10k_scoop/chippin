import React from "react";
import { StyleSheet, Text, View, TouchableOpacity, Image } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { AntDesign } from "@expo/vector-icons";
export default function Header(props) {
  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.backIcon} onPress={props.goBack}>
        <AntDesign name="arrowleft" size={rf(16)} color="black" />
      </TouchableOpacity>
      <View style={styles.title}>
        <Image
          source={require("../../../assets/LogoPic.png")}
          style={{ width: "50%", height: "45%", marginRight: "15%" }}
          resizeMode="contain"
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("100%"),
    height: hp("10%"),
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "space-evenly",
    paddingVertical: 5,
  },
  backIcon: {
    width: wp("6%"),
    height: wp("6%"),
    borderRadius: 100,
    borderWidth: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  title: {
    width: "80%",
    height: "100%",
    alignItems: "center",
    justifyContent: "flex-end",
  },
});
