import React from "react";
import { StyleSheet, Text, View, TextInput } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import { AntDesign } from "@expo/vector-icons";
export default function TextField(props) {
  return (
    <View style={styles.container}>
      <Text style={{ fontSize: rf(14) }}>{props.txt}</Text>
      <TextInput style={styles.TxtField} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("90%"),
    height: hp("12%"),
    justifyContent: "space-around",
  },
  TxtField: {
    width: "100%",
    height: "50%",
    backgroundColor: "#fff",
    borderRadius: 10,
    fontSize: rf(14),
    paddingHorizontal: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5,
  },
});
