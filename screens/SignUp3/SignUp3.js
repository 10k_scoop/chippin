import React from "react";
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Image,
  TouchableOpacity,
  ScrollView,
  TextInput,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import Header from "./components/Header";
import TextField from "./components/TextField";

export default function SignUp3({ navigation }) {
  return (
    <View style={styles.container}>
      <Header goBack={() => navigation.goBack()} />

      <View style={styles.Bubble}>
        <Image source={require("../../assets/Bubble.png")} />
      </View>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.Wrapper}>
          <View style={styles.Discription}>
            <Text style={styles.titleFont}>Additional info</Text>
            <Text style={styles.discFont}>
              Create a Chipin Charity Account and find oppurtunites to volunteer
              today
            </Text>
          </View>
          <TextField txt="phone number" />
          <TextField txt="tax id" />
          <View style={styles.About}>
            <Text>about charity</Text>
            <View style={styles.TextField}>
              <TextInput style={styles.InnerField} multiline={true} />
            </View>
          </View>
          <ImageBackground
            source={require("../../assets/BgPic.png")}
            style={styles.Image}
            resizeMode="cover"
          >
            <TouchableOpacity
              style={styles.Btn}
              onPress={() => navigation.navigate("DashboardCharity")}
            >
              <Text style={styles.btnFont}>next</Text>
            </TouchableOpacity>
          </ImageBackground>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#e3f7ff",
  },
  Wrapper: {
    width: wp("100%"),
    alignItems: "center",
  },
  Bubble: {
    position: "absolute",
    right: 0,
    top: -60,
    right: -70,
  },
  Discription: {
    width: wp("90%"),
    height: hp("15%"),
    justifyContent: "center",
  },
  titleFont: {
    fontSize: rf(22),
    fontWeight: "700",
    marginBottom: 10,
  },
  discFont: {
    fontSize: rf(16),
    fontWeight: "400",
  },
  Image: {
    width: wp("100%"),
    height: hp("16%"),
    alignItems: "center",
    justifyContent: "center",
  },
  Btn: {
    width: wp("90%"),
    height: hp("6%"),
    backgroundColor: "#fff",
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 5,
  },
  btnFont: {
    fontSize: rf(16),
    color: "grey",
  },
  About: {
    width: wp("90%"),
    height: hp("35%"),
    justifyContent: "space-around",
  },
  TextField: {
    width: "100%",
    height: "80%",
    backgroundColor: "#fff",
    borderRadius: 10,
    padding: 10,
  },
});
