import React from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import Header from "./components/Header";
import Card from "./components/Card";
import BottomMenu from "../../components/BottomMenu";

export default function PersonalDashboard({ navigation }) {
  return (
    <View style={styles.container}>
      <Header goBack={() => navigation.goBack()} />

      <View style={styles.Bubble}>
        <Image source={require("../../assets/Bubble.png")} />
      </View>
      <View style={styles.title}>
        <Text style={styles.titleFont}>Whats new</Text>
      </View>
      <Card btntxt="dismiss" sunny backgroundColor="#d77eea" />
      <Card btntxt="Volunteer now" hand backgroundColor="#ff8058" />
      <View style={styles.BgImage}>
        <Image
          source={require("../../assets/BgPic.png")}
          resizeMode="contain"
        />
      </View>
      <BottomMenu navigation={navigation} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#e3f7ff",
    alignItems: "center",
  },

  Bubble: {
    position: "absolute",
    right: 0,
    top: -50,
    right: -60,
  },
  title: {
    width: wp("90%"),
    height: hp("8%"),
    justifyContent: "center",
    marginBottom: "5%",
  },
  titleFont: {
    fontSize: rf(20),
    color: "#8945ff",
  },
  BgImage: {
    flex: 1,
    justifyContent: "flex-end",
  },
});
