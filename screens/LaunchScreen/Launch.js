import React from "react";
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Image,
  TouchableOpacity,
  ScrollView,
  TextInput,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";

export default function Launch({ navigation }) {
  return (
    <View style={styles.container}>
      <ImageBackground
        source={require("../../assets/LaunchBg.png")}
        style={{
          width: wp("100%"),
          height: hp("100%"),
          justifyContent: "flex-end",
          alignItems: "center",
        }}
        resizeMode="cover"
      >
        <TouchableOpacity
          style={styles.LogInBtn}
          onPress={() => navigation.navigate("Login")}
        >
          <Text style={{ fontSize: rf(16), color: "#fff" }}>Log in</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.SignInBtn}
          onPress={() => navigation.navigate("SignUp1")}
        >
          <Text style={{ fontSize: rf(16), color: "#0c66cc" }}>sign in</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.createAccount}
          onPress={() => navigation.navigate("Event1")}
        >
          <Text>Create charity account</Text>
        </TouchableOpacity>
      </ImageBackground>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#e3f7ff",
  },
  LogInBtn: {
    width: "90%",
    height: "7%",
    backgroundColor: "#0c66cc",
    borderRadius: 10,
    marginBottom: "4%",
    alignItems: "center",
    justifyContent: "center",
  },
  SignInBtn: {
    width: "90%",
    height: "7%",
    backgroundColor: "#fff",
    borderRadius: 10,
    marginBottom: "4%",
    alignItems: "center",
    justifyContent: "center",
  },
  createAccount: {
    borderBottomWidth: 1,
    marginBottom: hp("4%"),
  },
});
