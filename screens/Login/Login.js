import React from "react";
import {
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Image,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import Header from "./components/Header";
import TextField from "./components/TextField";
import PasswordField from "./components/PasswordField";

export default function Login({ navigation }) {
  return (
    <View style={styles.container}>
      <Header goBack={() => navigation.goBack()} />

      <View style={styles.Bubble}>
        <Image source={require("../../assets/Bubble.png")} />
      </View>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.Wrapper}>
          <View style={styles.Discription}>
            <Text style={styles.titleFont}>Create account</Text>
            <Text style={styles.discFont}>
              Create a Chipin Charity Account and find oppurtunites to volunteer
              today
            </Text>
          </View>
          <TextField txt="enter email address" />
          <PasswordField txt="create password" />
          <PasswordField txt="confirm password" />
          <ImageBackground
            source={require("../../assets/BgPic.png")}
            style={styles.Image}
            resizeMode="contain"
          >
            <TouchableOpacity
              style={styles.Btn}
              onPress={() => navigation.navigate("PersonalDashboard")}
            >
              <Text style={styles.btnFont}>Create account</Text>
            </TouchableOpacity>
          </ImageBackground>
        </View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#e3f7ff",
  },
  Wrapper: {
    width: wp("100%"),
    alignItems: "center",
  },
  Bubble: {
    position: "absolute",
    right: 0,
    top: -50,
    right: -70,
  },
  Discription: {
    width: wp("90%"),
    height: hp("18%"),
    justifyContent: "center",
  },
  titleFont: {
    fontSize: rf(22),
    fontWeight: "700",
    marginBottom: 10,
  },
  discFont: {
    fontSize: rf(16),
    fontWeight: "400",
  },
  Image: {
    width: wp("100%"),
    height: hp("52%"),
    alignItems: "center",
    justifyContent: "center",
  },
  Btn: {
    width: wp("90%"),
    height: hp("7%"),
    backgroundColor: "#0c66cc",
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 5,
  },
  btnFont: {
    fontSize: rf(16),
    color: "#fff",
  },
});
