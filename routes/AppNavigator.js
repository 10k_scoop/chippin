import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import Launch from "../screens/LaunchScreen/Launch";
import SignUp1 from "../screens/SignUp1/SignUp1";
import SignUp2 from "../screens/SignUp2/SignUp2";
import SignUp3 from "../screens/SignUp3/SignUp3";
import DashboardCharity from "../screens/Dashboard-Charity/DashboardCharity";
import Login from "../screens/Login/Login";
import PersonalDashboard from "../screens/Personal-Dashboard/PersonalDashboard";
import AccountPerson1 from "../screens/AccountPerson1/AccountPerson1";
import AccountPerson2 from "../screens/AccountPerson2/AccountPerson2";
import UserProfile from "../screens/User-Profile/UserProfile";
import EditProfile from "../screens/Edit-Profile/EditProfile";
import Event1 from "../screens/Event1/Event1";
import Event2 from "../screens/Event2/Event2";
import Event3 from "../screens/Event3/Event3";

const { Navigator, Screen } = createStackNavigator();

function AppNavigation() {
  return (
    <Navigator screenOptions={{ headerShown: false }}>
      <Screen name="Launch" component={Launch} />
      <Screen name="SignUp1" component={SignUp1} />
      <Screen name="SignUp2" component={SignUp2} />
      <Screen name="SignUp3" component={SignUp3} />
      <Screen name="DashboardCharity" component={DashboardCharity} />
      <Screen name="Login" component={Login} />
      <Screen name="PersonalDashboard" component={PersonalDashboard} />
      <Screen name="AccountPerson1" component={AccountPerson1} />
      <Screen name="AccountPerson2" component={AccountPerson2} />
      <Screen name="UserProfile" component={UserProfile} />
      <Screen name="EditProfile" component={EditProfile} />
      <Screen name="Event1" component={Event1} />
      <Screen name="Event2" component={Event2} />
      <Screen name="Event3" component={Event3} />
    </Navigator>
  );
}
export const AppNavigator = () => (
  <NavigationContainer>
    <AppNavigation />
  </NavigationContainer>
);
