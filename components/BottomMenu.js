import React from "react";
import { StyleSheet, Text, View, TouchableOpacity, Image } from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { RFValue as rf } from "react-native-responsive-fontsize";
import {
  SimpleLineIcons,
  MaterialCommunityIcons,
  Ionicons,
} from "@expo/vector-icons";
export default function BottomMenu(props) {
  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.Icon}>
        <SimpleLineIcons name="home" size={20} color="black" />
        <Text style={styles.font}>Home</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.Icon}
        onPress={() => props.navigation.navigate("AccountPerson1")}
      >
        <Ionicons name="hand-right-outline" size={20} color="black" />
        <Text style={styles.font}>create</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.Icon}
        onPress={() => props.navigation.navigate("AccountPerson2")}
      >
        <MaterialCommunityIcons
          name="calendar-blank-outline"
          size={20}
          color="black"
        />
        <Text style={styles.font}>upcoming</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.Icon}
        onPress={() => props.navigation.navigate("UserProfile")}
      >
        <MaterialCommunityIcons name="human-greeting" size={22} color="black" />
        <Text style={styles.font}>profile</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: wp("100%"),
    height: hp("7%"),
    position: "absolute",
    bottom: 0,
    flexDirection: "row",
  },
  Icon: {
    width: "25%",
    height: "100%",
    justifyContent: "space-evenly",
    alignItems: "center",
    paddingVertical: 5,
  },
  font: {
    fontSize: rf(12),
  },
});
